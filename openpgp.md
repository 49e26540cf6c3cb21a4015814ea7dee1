This is an OpenPGP proof that connects [my OpenPGP key](https://keyoxide.org/92E6FE25F62947A2E6DEAACD0087C595D44A8A10) to [this Github account](https://github.com/grantmeadors). For details check out https://keyoxide.org/guides/openpgp-proofs

[Verifying my OpenPGP key: openpgp4fpr:92E6FE25F62947A2E6DEAACD0087C595D44A8A10]